# Setup fzf
# ---------
if [[ ! "$PATH" == */home/valverde/.fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/valverde/.fzf/bin"
fi

# Auto-completion
# ---------------
source "/home/valverde/.fzf/shell/completion.zsh"

# Key bindings
# ------------
source "/home/valverde/.fzf/shell/key-bindings.zsh"
